﻿using System;
using PingPongerCommon;
using RabbitMQ.Client;
using RabbitMQ.Wrapper;
using RabbitMQ.Wrapper.Services;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var connection = new ConnectionFactory {Uri = Config.RabbitMqConnectionUri}.CreateConnection();
            using var channel = connection.CreateModel();
            
            var pingQueue = Common.PingQueue;
            var pongQueue = Common.PongQueue;
            var exchange = Common.Exchange;
            
            using var declare = new DeclarationService(channel);
            declare.ExchangeDeclare(exchange);
            declare.QueueDeclare(pingQueue);
            declare.QueueDeclare(pongQueue);
            
            using var bind = new BindingService(channel);
            bind.Bind(pongQueue, exchange, Common.PingPongNames.PongRoutingKey);
            bind.Bind(pingQueue, exchange, Common.PingPongNames.PingRoutingKey);
            
            using var publish = new PublishingService(channel, exchange, Common.PingPongNames.PingRoutingKey);
            var consume = new ConsumingService(channel, pongQueue, false);
            consume.Received += Common.ReceiveEventHandlerWithPublish(Common.PingPongNames.PingMessage, publish, consume);
            
            publish.Publish(Common.PingPongNames.PingMessage);
            Console.ReadKey();
        }
    }
}