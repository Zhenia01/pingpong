﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace PingPongerCommon
{
    public static class Common
    {
        public static class PingPongNames
        {
            public static string PongQueue { get; } = "pong_queue";
            public static string PingQueue { get; } = "ping_queue";
            public static string Exchange { get; } = "ping_pong";
            public static string PingRoutingKey { get; } = "ping";
            public static string PongRoutingKey { get; } = "pong";
            public static string PingMessage { get; } = "ping";
            public static string PongMessage { get; } = "pong";
        }
        
        public static Queue PingQueue => 
            new Queue(PingPongNames.PingQueue, isDurable: false, isAutoDelete: true);       
        
        public static Queue PongQueue => 
            new Queue(PingPongNames.PongQueue, isDurable: false, isAutoDelete: true);
        
        public static Exchange Exchange => 
            new Exchange(PingPongNames.Exchange, isDurable: false, isAutoDelete: true);

        public static EventHandler<BasicDeliverEventArgs> ReceiveEventHandlerWithPublish(string message, IPublishingService publish, IConsumingService consume)
        {
            return (ch, ea) =>
            {
                var messageIn = Encoding.UTF8.GetString(ea.Body.ToArray());
                Console.WriteLine($"Received {messageIn} at {DateTime.Now}");
                
                Thread.Sleep(TimeSpan.FromSeconds(2.5));
                
                publish.Publish(message);
                Console.WriteLine($"Sent {message} at {DateTime.Now}");
                
                consume.SetAcknowledge(ea.DeliveryTag, true);
            };
        }
    }
}