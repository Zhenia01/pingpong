using System;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IConsumingService
    {
        void SetAcknowledge(ulong deliveryTag, bool processed);
        event EventHandler<BasicDeliverEventArgs> Received;
    }
}