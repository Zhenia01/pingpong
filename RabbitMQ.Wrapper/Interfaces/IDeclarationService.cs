using System;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IDeclarationService : IDisposable
    {
        void QueueDeclare(Queue queue);
        void ExchangeDeclare(Exchange exchange);
        
        IModel Channel { get; }
    }
}