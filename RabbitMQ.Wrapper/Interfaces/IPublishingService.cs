namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IPublishingService
    {
        void Publish(string message, string type = null);
    }
}