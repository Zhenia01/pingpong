using System;
using System.Collections.Generic;

namespace RabbitMQ.Wrapper.Models
{
    public class Queue
    {
        public Queue(
            string name,
            bool isDurable = false,
            bool isExclusive = false,
            bool isAutoDelete = false,
            IDictionary<string, object> arguments = null)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name), "Queue name must not be null");
            IsDurable = isDurable;
            IsExclusive = isExclusive;
            IsAutoDelete = isAutoDelete;
            Arguments = arguments ?? new Dictionary<string, object>();
        }

        public string Name { get; }
        public bool IsDurable { get; }
        public bool IsExclusive { get; }
        public bool IsAutoDelete { get; }
        public IDictionary<string, object> Arguments { get; }
    }
}