using System;
using System.Collections.Generic;
using RabbitMQ.Client;

namespace RabbitMQ.Wrapper.Models
{
    public class Exchange
    {
        public Exchange(
            string name,
            string type = ExchangeType.Direct,
            bool isDurable = true,
            bool isAutoDelete = false,
            IDictionary<string, object> arguments = null)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name), "Exchange name must not be null");
            Type = type;
            IsDurable = isDurable;
            IsIsAutoDelete = isAutoDelete;
            Arguments = arguments ?? new Dictionary<string, object>();
        }

        public string Name { get; }
        public string Type { get; }
        public bool IsDurable { get; }
        public bool IsIsAutoDelete { get; }
        public IDictionary<string, object> Arguments { get; }
    }
}