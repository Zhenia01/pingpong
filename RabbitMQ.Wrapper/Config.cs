using System;

namespace RabbitMQ.Wrapper
{
    public static class Config
    {
        public static Uri RabbitMqConnectionUri { get; } = new Uri("amqp://guest:guest@localhost:5672");
    }
}