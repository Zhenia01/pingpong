using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class PublishingService : IPublishingService, IDisposable
    {
        private readonly IModel _channel;
        private readonly IBasicProperties _properties;
        private readonly string _routingKey;
        private readonly string _exchangeName;
        
        public PublishingService(IModel channel, string exchangeName, string routingKey)
        {
            _exchangeName = exchangeName ?? throw new ArgumentNullException(nameof(exchangeName), "Exchange name must not be null");
            _channel = channel;
            _properties = _channel.CreateBasicProperties();
            _routingKey = routingKey;
        }
        
        public PublishingService(IModel channel, Exchange exchange, string routingKey) : this(channel, exchange.Name, routingKey)
        {
        }
        
        public void Publish(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            
            _channel.BasicPublish(_exchangeName, _routingKey, _properties, body);
        }

        private bool _disposed;
        public void Dispose()
        {
            if (!_disposed)
            {
                _channel.Dispose();
                _disposed = true;
            }
        }
    }
}