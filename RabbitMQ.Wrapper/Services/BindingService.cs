using System;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class BindingService : IBindingService
    {
        public IModel Channel { get; }

        public BindingService(IModel channel)
        {
            Channel = channel;
        }

        public void Bind(Queue queue, Exchange exchange, string routingKey)
        {
            Bind(queue.Name, exchange.Name, routingKey);
        }

        public void Bind(string queueName, string exchangeName, string routingKey)
        {
            if (queueName == null) throw new ArgumentNullException(nameof(queueName), "Queue name must not be null");
            if (exchangeName == null) throw new ArgumentNullException(nameof(exchangeName), "Exchange name must not be null");
            
            routingKey ??= string.Empty;
            
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        private bool _disposed;
        
        public void Dispose()
        {
            if (!_disposed)
            {
                Channel.Dispose();
                _disposed = true;
            }
        }
    }
}