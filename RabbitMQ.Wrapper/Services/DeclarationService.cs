using System;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class DeclarationService : IDeclarationService
    {
        public IModel Channel { get; }
        
        public DeclarationService(IModel channel)
        {
            Channel = channel;
        }
        
        public void QueueDeclare(Queue queue)
        {
            Channel.QueueDeclare(queue.Name, queue.IsDurable, queue.IsExclusive, queue.IsAutoDelete, queue.Arguments);
        }

        public void ExchangeDeclare(Exchange exchange)
        {
            Channel.ExchangeDeclare(exchange.Name, exchange.Type, exchange.IsDurable, exchange.IsIsAutoDelete, exchange.Arguments);
        }

        private bool _disposed;
        
        public void Dispose()
        {
            if (!_disposed)
            {
                Channel.Dispose();
                _disposed = true;
            }
        }
    }
}