using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class ConsumingService : IConsumingService
    {
        private readonly EventingBasicConsumer _consumer;

        public ConsumingService(IModel channel, string queueName, bool isAutoAck = true)
        {
            string queueName1 = queueName ?? throw new ArgumentNullException(nameof(queueName), "Queue name must not be null");
            _consumer = new EventingBasicConsumer(channel);
            channel.BasicConsume(queueName1, isAutoAck, _consumer);
        }
        
        public ConsumingService(IModel channel, Queue queue, bool isAutoAck) : this(channel, queue.Name, isAutoAck)
        {
        }
        
        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed)
            {
                _consumer.Model.BasicAck(deliveryTag, false);
            }
            else
            {
                _consumer.Model.BasicNack(deliveryTag, false, true);
            }
        }

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }
    }
}